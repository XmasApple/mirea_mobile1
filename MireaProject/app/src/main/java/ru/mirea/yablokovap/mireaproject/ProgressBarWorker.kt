package ru.mirea.yablokovap.mireaproject

import android.content.Context
import android.util.Log
import android.widget.ProgressBar
import androidx.work.Worker
import androidx.work.WorkerParameters
import java.util.concurrent.TimeUnit


class ProgressBarWorker(
    context: Context,
    params: WorkerParameters,
) : Worker(context, params) {
    private val tag = "ProgressBarWorker"

    override fun doWork(): Result {
        Log.d(tag, "doWork: start")
        try {
                TimeUnit.SECONDS.sleep(5)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        Log.d(tag, "doWork: end")
        return Result.success()
    }
}