package ru.mirea.yablokovap.mireaproject

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.ImageView


class Model(internal var uri: Uri)
class ImageAdapter(context: Context, modelArrayList: ArrayList<Model?>?) :
    ArrayAdapter<Model?>(context, 0, modelArrayList!!) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val listView =
            convertView ?: LayoutInflater.from(context).inflate(R.layout.camera_card, parent, false)

        val model: Model? = getItem(position)
        val imageView = listView.findViewById<ImageView>(R.id.imageView)

        imageView.setImageURI(model!!.uri)
        return listView
    }
}
