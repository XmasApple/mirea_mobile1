package ru.mirea.yablokovap.camera

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import ru.mirea.yablokovap.camera.databinding.ActivityMainBinding
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


private const val REQUEST_CODE_PERMISSION = 100

class MainActivity : AppCompatActivity() {
    private val tag = this::class.simpleName
    private lateinit var binding: ActivityMainBinding

    private var isWork = false
    private lateinit var imageUri: Uri

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.getRoot())

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val cameraPermissionStatus =
            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
        val storagePermissionStatus =
            ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES)
        Log.d(tag, "cameraPermissionStatus: $cameraPermissionStatus")
        Log.d(tag, "storagePermissionStatus: $storagePermissionStatus")

        if (cameraPermissionStatus == PackageManager.PERMISSION_GRANTED
             && storagePermissionStatus == PackageManager.PERMISSION_GRANTED) {
            Log.d(tag, "PERMISSON GRANTED")
            isWork = true
        } else {
            Log.d(tag, "PERMISSON NOT GRANTED")

            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.READ_MEDIA_IMAGES
                ),
                REQUEST_CODE_PERMISSION
            )
        }

        val callback: ActivityResultCallback<ActivityResult> =
            ActivityResultCallback { result ->
                if (result.resultCode == RESULT_OK) {
                    val data = result.data
                    binding.image.setImageURI(imageUri)
                }
            }

        val cameraActivityResultLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            callback
        )
        //	Обработчик	нажатия	на	компонент	«imageView»
        binding.btn.setOnClickListener {
            Log.d(tag, "MediaStore.ACTION_IMAGE_CAPTURE")
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            //	проверка на наличие разрешений для камеры
            if (isWork) try {
                val photoFile = createImageFile()
                //	генерирование пути к файлу на основе authorities
                val authorities = "${applicationContext.packageName}.fileprovider"
                imageUri = FileProvider.getUriForFile(
                    this@MainActivity,
                    authorities,
                    photoFile
                )
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
                cameraActivityResultLauncher.launch(cameraIntent)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Производится генерирование имени файла на основе текущего времени и создание файла
     * в директории Pictures на ExternelStorage.
     * class.
     * @return File возвращается объект File .
     * @exception IOException если возвращается ошибка записи в файл
     */
    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
        val imageFileName = "IMAGE_${timeStamp}_"
        val storageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(imageFileName, ".jpg", storageDirectory)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        //	производится проверка полученного результата от пользователя на запрос разрешения Camera
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSION) {
            //	permission	granted
            isWork = (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        }
    }
}