package ru.mirea.yablokovap.lesson6

import android.os.Bundle
import android.widget.EditText
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import ru.mirea.yablokovap.lesson6.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()

        binding = ActivityMainBinding.inflate(layoutInflater);
        setContentView(binding.root);

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val sharedPref = getSharedPreferences("mirea_settings", MODE_PRIVATE)
        binding.groupEditText.setText(sharedPref.getString("GROUP", ""))
        binding.numberEditText.setText(sharedPref.getInt("NUMBER", 0).toString())
        binding.filmEditText.setText(sharedPref.getString("FILM", ""))

        binding.saveBtn.setOnClickListener {
            val sharedPrefOnSave = getSharedPreferences("mirea_settings", MODE_PRIVATE)

            val editor = sharedPrefOnSave.edit()

            editor.putString("GROUP", binding.groupEditText.text.toString())
            editor.putInt("NUMBER", binding.numberEditText.text.toString().toInt())
            editor.putString("FILM", binding.filmEditText.text.toString())
            editor.apply()
        }
    }
}