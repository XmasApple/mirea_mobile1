package ru.mirea.yablokovap.employeedb

import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class MainActivity : AppCompatActivity() {
    private val tag = this::class.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        Log.d(tag, "1")
        val db: AppDatabase? = App.instance?.database

        val superHeroDao: SuperHeroDao? = db?.employeeDao()
        val superHero1 = SuperHero(
            name = "Foo Bar",
            strength = 1_000_000
        )

        val superHero2 = SuperHero(
            name = "Fizz Buzz",
            strength = 100_500
        )
        superHeroDao?.insert(superHero1)
        superHeroDao?.insert(superHero2)

        val sh1 = superHeroDao?.getById(1)
        sh1?.strength = 2_000_000
        superHeroDao?.update(sh1)

        val superHeroes: List<SuperHero>? = superHeroDao?.all

        superHeroes?.forEach {
            Log.d(tag, "${it.name}: ${it.strength}")
        }
    }
}