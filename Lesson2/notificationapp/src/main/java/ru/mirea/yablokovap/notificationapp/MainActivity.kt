package ru.mirea.yablokovap.notificationapp

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {

    private val tag = this::class.simpleName
    private val CHANNEL_ID = "com.mirea.asd.notification.ANDROID";

    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val requestPermissionLauncher =
            registerForActivityResult(
                ActivityResultContracts.RequestPermission()
            ) { isGranted: Boolean ->
                if (isGranted) {
                    Log.d(tag, "PERMISSION GRANTED")
                } else {
                    Log.d(tag, "PERMISSION NOT GRANTED")
                }
            }

        when {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.POST_NOTIFICATIONS
            ) == PackageManager.PERMISSION_GRANTED -> {
                Log.d(tag, "PERMISSION GRANTED")
            }

            ActivityCompat.shouldShowRequestPermissionRationale(
                this, Manifest.permission.POST_NOTIFICATIONS
            ) -> {
            }

            else -> {
                requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }

    fun onClickNewMessageNotification(view: View) {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        )
            return

        NotificationManagerCompat.from(this).also {
            it.createNotificationChannel(NotificationChannel(
                CHANNEL_ID,
                "Yablokov A.P. Notification",
                NotificationManager.IMPORTANCE_DEFAULT
            )
                .also { it.description = "MIREA CHANNEL!" })
            it.notify(
                1,
                NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentText("Lol")
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setStyle(
                        NotificationCompat.BigTextStyle().bigText(
                            "A lot of lollolololol inmultiline text lol"
                        )
                    )
                    .setContentTitle("MIREA lol")
                    .build()
            )
        }
    }
}