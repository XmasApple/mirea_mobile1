package ru.mirea.yablokovap.toastapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClickToast(view: View) {
        val editText: EditText = findViewById(R.id.editText)
        val toast = Toast.makeText(
            applicationContext,
            "СТУДЕНТ №31 ГРУППА БСБО-10-21 Количество символов - ${editText.text.length}",
            Toast.LENGTH_SHORT
        )
        toast.show()
    }
}