package ru.mirea.yablokovap.dialog

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.DialogFragment


class MyTimeDialogFragment : TimePickerDialog {
    constructor(
        context: Context?,
        listener: OnTimeSetListener?,
        hourOfDay: Int,
        minute: Int,
        is24HourView: Boolean
    ) : super(context, listener, hourOfDay, minute, is24HourView)

    constructor(
        context: Context?,
        themeResId: Int,
        listener: OnTimeSetListener?,
        hourOfDay: Int,
        minute: Int,
        is24HourView: Boolean
    ) : super(context, themeResId, listener, hourOfDay, minute, is24HourView)
}


class MyDateDialogFragment : DatePickerDialog {
    constructor(context: Context) : super(context)
    constructor(context: Context, themeResId: Int) : super(context, themeResId)
    constructor(
        context: Context,
        listener: OnDateSetListener?,
        year: Int,
        month: Int,
        dayOfMonth: Int
    ) : super(context, listener, year, month, dayOfMonth)

    constructor(
        context: Context,
        themeResId: Int,
        listener: OnDateSetListener?,
        year: Int,
        monthOfYear: Int,
        dayOfMonth: Int
    ) : super(context, themeResId, listener, year, monthOfYear, dayOfMonth)
}


class MyProgressDialogFragment : ProgressDialog {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, theme: Int) : super(context, theme)
}

class AlertDialogFragment : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        AlertDialog.Builder(activity)
            .setTitle("Hi mirea")
            .setMessage("Yes?")
            .setIcon(R.mipmap.ic_launcher_round)
            .setPositiveButton("Next")
            { dialog, _ ->
                run {
                    onOkClicked()
                    dialog.cancel()
                }
            }
            .setNegativeButton("No")
            { dialog, _ ->
                run {
                    onCancelClicked()
                    dialog.cancel()
                }
            }
            .setNeutralButton("Pause")
            { dialog, _ ->
                run {
                    onNeutralClicked()
                    dialog.cancel()
                }
            }
            .create()

    private fun onOkClicked() =
        Toast.makeText(
            context,
            "Button  \"Next\" clicked!",
            Toast.LENGTH_LONG
        ).show()


    private fun onCancelClicked() =
        Toast.makeText(
            context,
            "Button  \"No\" clicked!",
            Toast.LENGTH_LONG
        ).show()

    private fun onNeutralClicked() =
        Toast.makeText(
            context,
            "Button  \"Pause\" clicked!",
            Toast.LENGTH_LONG
        ).show()
}