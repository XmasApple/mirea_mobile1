package ru.mirea.yablokovap.dialog

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClickShowAlertDialog(view: View) {
        val fragment = AlertDialogFragment()
        fragment.show(supportFragmentManager, "mirea")
    }

    fun onClickShowTimeDialog(view: View) = MyTimeDialogFragment(
        this,
        { _: TimePicker?, hours: Int, minutes: Int ->
            Log.d(
                "TIME TAG",
                String.format("Chosen: %d:%d", hours, minutes)
            )
        }, 10, 20, true
    ).show()

    fun onClickShowDateDialog(view: View) =
        MyDateDialogFragment(
            this,
            { _: DatePicker?, year: Int, month: Int, day: Int ->
                Snackbar.make(view, "${day}.${month}.${year}", Snackbar.LENGTH_LONG).show()
            }, 2022, 12, 28
        ).show()


    fun onClickShowProgressDialog(view: View) {
        MyProgressDialogFragment(this).run {
            setTitle("TITLE")
            setMessage("Loading...")
            show()
        }

    }
}