package ru.mirea.yablokovap.buttonclicker

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private lateinit var textViewStudent: TextView
    private lateinit var checkBox: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textViewStudent = findViewById(R.id.textViewStudent)
        checkBox = findViewById(R.id.checkBox)

        val btnWhoAmI = findViewById<Button>(R.id.btnWhoAmI)

        btnWhoAmI.setOnClickListener {
            textViewStudent.text = "My num is 31"
        }
    }

    fun onBtnItsNotMeClick(view: View?) {
        textViewStudent.text = "Not me!"
        Toast.makeText(this, "Not me!", Toast.LENGTH_SHORT).show()
        checkBox.isChecked = !checkBox.isChecked
    }

}