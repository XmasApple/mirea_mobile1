package ru.mirea.yablokovap.layouttype

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textView = findViewById<TextView>(R.id.mainTextView)
        val button = findViewById<Button>(R.id.mainButton)
        val checkBox = findViewById<CheckBox>(R.id.mainCheckBox)

        textView.text = "Text Mirea"
        button.text = "Mirea Button"
        checkBox.text = "Test"
        checkBox.isChecked = true
    }
}