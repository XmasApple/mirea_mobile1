package ru.mirea.yablokovap.simplefragmentapp

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;


class MainActivity : AppCompatActivity() {
    private lateinit var fragment1: Fragment
    private lateinit var fragment2: Fragment
    private lateinit var fragmentManager: FragmentManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragment1 = FirstFragment()
        fragment2 = SecondFragment()
    }

    fun onClick(view: View) {
        fragmentManager = supportFragmentManager

        val firstFr: Int = R.id.btnFirstFragment
        val secondFr: Int = R.id.btnSecondFragment
        val fragment = when (view.id) {
            firstFr -> fragment1
            secondFr -> fragment2
            else -> return
        }
        fragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment).commit()
    }


}