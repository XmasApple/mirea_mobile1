package ru.mirea.yablokovap.favoritebook

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat


class MainActivity : AppCompatActivity() {

    private lateinit var activityResultLauncher: ActivityResultLauncher<Intent>


    companion object {
        val KEY = "book_name"
        val USER_MESSAGE = "MESSAGE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val mainText = findViewById<TextView>(R.id.textView)
        val callback: ActivityResultCallback<ActivityResult> =
            ActivityResultCallback { result ->
                if (result.resultCode == RESULT_OK) {
                    val data: Intent? = result.data
                    val userBook = data?.getStringExtra(USER_MESSAGE)
                    val text = "Название вашей любимой книги: $userBook"
                    mainText.text = text
                }
            }
        activityResultLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult(),
            callback
        )
    }

    fun getBookInfo(view: View?) {
        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra(KEY, "Нету ¯\\_(ツ)_/¯")
        activityResultLauncher.launch(intent)
    }
}