package ru.mirea.yablokovap.favoritebook

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat


class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_second)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }


        val textView = findViewById<TextView>(R.id.textView)
        var text: String? = textView.getText().toString()
        text += intent.getStringExtra(MainActivity.KEY)
        textView.text = text
    }

    fun sendData(view: View) {
        val editText = findViewById<EditText>(R.id.editText)
        val data = Intent()
        data.putExtra(MainActivity.USER_MESSAGE, editText.getText().toString())
        setResult(RESULT_OK, data)
        finish()
    }
}