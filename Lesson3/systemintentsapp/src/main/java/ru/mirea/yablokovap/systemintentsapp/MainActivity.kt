package ru.mirea.yablokovap.systemintentsapp

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }
    private fun launchIntent(action: String, dataUri: String) {
        val intent = Intent(action)
        intent.data = Uri.parse(dataUri)
        startActivity(intent)
    }

    fun onBtnCallClick(view: View) {
        launchIntent(Intent.ACTION_DIAL, "tel:89811112233")
    }

    fun onBtnWebClick(view: View) {
        launchIntent(Intent.ACTION_VIEW, "http://developer.android.com")
    }

    fun onBtnMapsClick(view: View) {
        launchIntent(Intent.ACTION_VIEW, "geo:55.749479,37.613944")
    }

}