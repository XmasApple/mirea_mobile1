package ru.mirea.yablokovap.cryptoloader

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.loader.app.LoaderManager
import androidx.loader.content.Loader
import ru.mirea.yablokovap.cryptoloader.databinding.ActivityMainBinding
import java.security.InvalidKeyException
import java.security.InvalidParameterException
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import javax.crypto.BadPaddingException
import javax.crypto.Cipher
import javax.crypto.IllegalBlockSizeException
import javax.crypto.KeyGenerator
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec


class MainActivity : AppCompatActivity(), LoaderManager.LoaderCallbacks<String> {
    private lateinit var binding: ActivityMainBinding
    private val tag = this::class.simpleName
    private val loaderID = 1234
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.getRoot())

        binding.button.setOnClickListener {
            val bundle = Bundle().also {
//                it.putString(MyLoader.ARG_WORD, "mirea")
                val key = generateKey()
                it.putByteArray(
                    MyLoader.ARG_WORD,
                    encryptMsg(binding.textEdit.text.toString(), key)
                )
                it.putByteArray("key", key.encoded)
            }
            LoaderManager.getInstance(this).initLoader(
                loaderID,
                bundle,
                this
            )
        }
    }

    override fun onCreateLoader(id: Int, args: Bundle?): Loader<String> {
        if (id == loaderID) {
            Toast.makeText(this, "onCreateLoader $id", Toast.LENGTH_SHORT).show()
            return MyLoader(this, args)
        }
        throw InvalidParameterException("Invalid loader id")
    }

    override fun onLoaderReset(loader: Loader<String>) {
        Log.d(tag, "onLoaderReset")
    }

    override fun onLoadFinished(loader: Loader<String>, data: String) {
        if (loader.id == loaderID) {
            Log.d(tag, "onLoadFinished $data")
            Toast.makeText(this, "onLoadFinished $data", Toast.LENGTH_SHORT).show()
        }
    }

    fun generateKey(): SecretKey {
        return try {
            val kg = KeyGenerator.getInstance("AES")
                .also {
                    it.init(256, SecureRandom.getInstance("SHA1PRNG").also { it1 ->
                        it1.setSeed("any data used as random seed".toByteArray())
                    })
                }
            SecretKeySpec(kg.generateKey().encoded, "AES")
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException(e)
        }
    }

    fun encryptMsg(message: String, secret: SecretKey?): ByteArray {
        val cipher: Cipher?
        return try {
            cipher = Cipher.getInstance("AES")
            cipher.init(Cipher.ENCRYPT_MODE, secret)
            cipher.doFinal(message.toByteArray())
        } catch (e: NoSuchAlgorithmException) {
            throw RuntimeException(e)
        } catch (e: NoSuchPaddingException) {
            throw RuntimeException(e)
        } catch (e: InvalidKeyException) {
            throw RuntimeException(e)
        } catch (e: BadPaddingException) {
            throw RuntimeException(e)
        } catch (e: IllegalBlockSizeException) {
            throw RuntimeException(e)
        }
    }
}