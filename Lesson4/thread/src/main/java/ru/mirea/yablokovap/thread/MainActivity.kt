package ru.mirea.yablokovap.thread

import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import ru.mirea.yablokovap.thread.databinding.ActivityMainBinding
import kotlin.random.Random


class MainActivity : AppCompatActivity() {
    private val tag = this::class.simpleName
    private val threadTag = "ThreadProject"
    private val group = "БСБО-10-21"
    private val listNum = 31
    private lateinit var binding: ActivityMainBinding
    private var threadCounter: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.getRoot())

        val mainThread = Thread.currentThread()
        binding.threadInfo.text = "Thread name: ${mainThread.name}"
        mainThread.name = "МОЙ НОМЕР ГРУППЫ: 10, НОМЕР ПО СПИСКУ: 31, МОЙ ЛЮБИМЫЙ ФИЛЬМ: хз"
        binding.threadInfo.append("\nNew thread name: ${mainThread.name}")
        Log.d(tag, "Stack: ${mainThread.getStackTrace().contentToString()}")


        binding.button.setOnClickListener {
            Thread(Runnable {
                val threadNum = threadCounter++
                Log.d(
                    threadTag,
                    "Запущен поток №$threadNum " +
                            "студентом группы $group " +
                            "номер по списку №$listNum"
                )
                runOnUiThread {
                    binding.textView.text = "wait"
                    binding.button.isEnabled = false
                }
                val classes = Random.nextInt(10, 30)
                val delay = Random.nextFloat() * 5 + 1;
                Thread.sleep((delay * 1000).toLong());
                runOnUiThread {
                    binding.textView.text = "$classes classes"
                    binding.button.isEnabled = true
                }
                Log.d(threadTag, "Выполнен поток №$threadNum")
            }).start()
        }
    }
}