package ru.mirea.yablokovap.workmanager

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import java.util.concurrent.TimeUnit


class UploadWorker(
    context: Context,
    params: WorkerParameters
) : Worker(context, params) {
    private val tag = this::class.simpleName

    override fun doWork(): Result {
        Log.d(tag, "doWork: start")
        try {
            TimeUnit.SECONDS.sleep(10)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        Log.d(tag, "doWork: end")
        return Result.success()
    }

}