package ru.mirea.yablokovap.serviceapp

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat

const val CHANNEL_ID = "ForegroundServiceChannel"

class PlayerService : Service() {
    private lateinit var mediaPlayer: MediaPlayer
    private val tag = this::class.simpleName

    override fun onBind(intent: Intent?): IBinder {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        mediaPlayer.start()
        Log.d(tag, "onStartCommand")

        mediaPlayer.setOnCompletionListener { stopForeground(true); Log.d(tag, "stopForeground") }
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onCreate() {
        super.onCreate()
        val builder: NotificationCompat.Builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentText("Playing....")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setStyle(
                NotificationCompat.BigTextStyle()
                    .bigText("best player...")
            )
            .setContentTitle("Music Player")
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(
            CHANNEL_ID, "Student YablokovAP Notification", importance
        )
        channel.description = "MIREA Channel"
        val notificationManager = NotificationManagerCompat.from(this)
        notificationManager.createNotificationChannel(channel)
        startForeground(1, builder.build())
        mediaPlayer = MediaPlayer.create(this, R.raw.file_example_mp3_700kb)
        mediaPlayer.isLooping = false
    }

    override fun onDestroy() {
        stopForeground(true)
        mediaPlayer.stop()
        Log.d(tag, "onDestroy")
    }
}