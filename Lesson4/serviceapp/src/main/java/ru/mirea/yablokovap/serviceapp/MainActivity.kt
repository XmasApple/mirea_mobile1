package ru.mirea.yablokovap.serviceapp

import android.Manifest.permission.FOREGROUND_SERVICE
import android.Manifest.permission.POST_NOTIFICATIONS
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import ru.mirea.yablokovap.serviceapp.databinding.ActivityMainBinding


const val PLAY = "▶"
const val STOP = "◼"

const val PERMISSION_CODE = 200

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private var isPaused = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        enableEdgeToEdge()
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        if (ContextCompat.checkSelfPermission(
                this,
                POST_NOTIFICATIONS
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            Log.d(MainActivity::class.java.getSimpleName().toString(), "Разрешения получены")
        } else {
            Log.d(MainActivity::class.java.getSimpleName().toString(), "Нет разрешений!")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                ActivityCompat.requestPermissions(
                    this, arrayOf(POST_NOTIFICATIONS, FOREGROUND_SERVICE), PERMISSION_CODE
                )
            }
        }

        binding.pause.setOnClickListener {
            if (isPaused) {
                (it as Button).text = STOP
                ContextCompat.startForegroundService(
                    this@MainActivity,
                    Intent(this@MainActivity, PlayerService::class.java)
                )
            } else {
                (it as Button).text = PLAY
                stopService(
                    Intent(this@MainActivity, PlayerService::class.java)
                )
            }
            isPaused = !isPaused
        }
    }
}