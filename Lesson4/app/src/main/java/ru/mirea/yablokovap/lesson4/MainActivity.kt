package ru.mirea.yablokovap.lesson4

import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import ru.mirea.yablokovap.lesson4.databinding.ActivityMainBinding

const val PLAY = "▶"
const val STOP = "◼"

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val tag = this::class.simpleName

    private var isPaused = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.getRoot())


        binding.prevSong.setOnClickListener {
            Log.i(tag, "prevSong clk")
        }
        binding.pause.setOnClickListener {
            isPaused = !isPaused
            (it as Button).text = (if (isPaused) PLAY else STOP)
            Log.i(tag, "pause clk")
        }
        binding.nextSong.setOnClickListener {
            Log.i(tag, "nextSong clk")
        }
    }
}