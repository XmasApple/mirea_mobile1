package ru.mirea.yablokovap.looper

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log


class MyLooper(mainThreadHandler: Handler) : Thread() {
    lateinit var mHandler: Handler
    private val mainHandler: Handler

    init {
        mainHandler = mainThreadHandler
    }

    override fun run() {
        Log.d("ru.mirea.yablokovap.looper.MyLooper", "run")
        Looper.prepare()
        mHandler = object : Handler(Looper.myLooper()!!) {
            override fun handleMessage(msg: Message) {
                val data: String = msg.getData().getString("KEY")!!
                Log.d("ru.mirea.yablokovap.looper.MyLooper get message: ", data)
                val count = data.length
                val message = Message()
                Bundle().also {
                    it.putString(
                        "result",
                        String.format("The number of letters in the word $data is $count ")
                    )
                    message.data = it
                }
                mainHandler.sendMessageDelayed(message, 20000)
            }
        };
        Looper.loop()
    }
}