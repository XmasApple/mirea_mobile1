package ru.mirea.yablokovap.looper

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import ru.mirea.yablokovap.looper.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val tag = this::class.simpleName
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.getRoot())

        val mainThreadHandler: Handler = object : Handler(Looper.getMainLooper()) {
            override fun handleMessage(msg: Message) {
                Log.d(tag, "Task execute. This is result: ${msg.data.getString("result")}")
            }
        }
        val myLooper = MyLooper(mainThreadHandler)
        myLooper.start()

        binding.textView.text = "Мой номер по списку №31"
        binding.button.setOnClickListener {
            val msg = Message.obtain()
            Bundle().also {
                it.putString("KEY", "Age: ${binding.age.text}; Profession: ${binding.work.text}")
                msg.data = it
            }
            myLooper.mHandler.sendMessage(msg)
        }
    }
}